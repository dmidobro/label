import React from "react"
import Logo from './mozoLogo.png'
let QRCode = require('qrcode.react');

const data = [
    {
        title: "0",
        complexity: '3A',
        name: 'Паукаев',
        qrCode: "c7a41892-43e9-464a-9e09-a470b74340e4"
    },

    {
        title: "1",
        complexity: '6A',
        name: 'Дмитровский',
        qrCode: "c7a41892-43e9-464a-9e09-a470b74340e4"
    },
    {
        title: "2",
        complexity: '5C',
        name: 'Гагарин',
        qrCode: "c7a41892-43e9-464a-9e09-a470b74340e4"
    },
    {
        title: "3",
        complexity: '3A',
        name: 'Добренко',
        qrCode: "c7a41892-43e9-464a-9e09-a470b74340e4"
    },

    {
        title: "4",
        complexity: '6A',
        name: 'Сало',
        qrCode: "c7a41892-43e9-464a-9e09-a470b74340e4"
    },
    {
        title: "5",
        complexity: '5C',
        name: 'Армстронг',
        qrCode: "c7a41892-43e9-464a-9e09-a470b74340e4"
    },
];

export default function Grid() {
    return (
        <div style={styles.body}>
        <div style={styles.grid} className="grid">
            {data.map(item => (
                <Cell {...item} />
            ))}
        </div>
        </div>
    );
}

const Cell = props => {
    return (
        <div style={styles.cell}>
            <div style={styles.box}>
                <div style={styles.logo}>Powered by <img src={Logo} style={styles.logoimg}/></div>
                <header style={styles.header}>{props.title}</header>
                <hr
                    style={{
                        width: 135,
                        borderColor: '#000000'
                    }}
                />
                <div style={{fontSize: 50}}>{props.complexity}</div>
                <div style={{fontSize: 25, margin: 5}}>{props.name}</div>
                <footer style={styles.footer}><QRCode
                    id="0"
                    value={props.qrCode}
                    size={150}
                    level={"H"}
                    includeMargin={true}
                /></footer>
            </div>
        </div>
    );
};

const styles = {
    grid: {
        fontFamily: "sans-serif",
        textAlign: "center",
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        width: "550px",
        // width: '100%',
        justifyContent: "center",
        // boxSizing: "border-box",
    },
    cell: {
        boxSizing: "border-box",
        flex: "0 0 33%",
        height: "400px",
        // border: "1px solid black",
        padding: "5px",
        display: "flex",
        justifyContent: "center",
    },
    box: {

        width: "100%",
        borderRadius: "30px",
        border: "3px solid black",
        display: "flex",
        flexDirection: "column",
    },
    header: {
        fontSize: 55
    },
    logo: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '10px',
        width: '100%',
        margin: '7px',
    },
    logoimg: {
        width: '25px',
        height: "25px",
    },
    // body: {
    //     width: "100%",
    //     height: "100%"
    // }
};

